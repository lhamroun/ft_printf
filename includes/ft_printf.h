/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/08 22:08:46 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/03/07 07:05:31 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <unistd.h>
# include <stdlib.h>
# include "../libft/libft.h"

typedef struct	s_pos_n_len
{
	int		p;
	int		l;
}				t_pos_n_len;

typedef struct	s_flg
{
	int		h;
	int		less;
	int		plus;
	int		z;
	int		s;
	int		w;
	int		p;
	int		t;
}				t_flg;

typedef struct	s_aff
{
	int		z_arg;
	char	sign1;
	int		s1;
	int		z1;
	char	sign2;
	int		s2;
	int		neg;
}				t_aff;

int				ft_printf(char const *format, ...);
t_pos_n_len		parse_pos_len(char const *format, t_pos_n_len *pl, va_list ap);
void			pars_flag(t_flg *flg, char const *f, int i, int j);
int				pars_format_flag(t_flg *flg, char const *f, int i, int j);
int				flag_type(char const *f, int i, int j);
t_pos_n_len		error(t_pos_n_len pl);
int				find_arg_n_aff(char var, va_list ap, t_flg flg);
void			putfloat_precis(float f, int p);

int				convert_int_to_l(char c, t_flg flg, va_list ap);
int				convert_int_to_ll(char c, t_flg flg, va_list ap);
int				convert_int_to_h(char c, t_flg flg, va_list ap);
int				convert_int_to_hh(char c, t_flg flg, va_list ap);
int				convert_int_to_j(char c, t_flg flg, va_list ap);
int				convert_int_to_z(char c, t_flg flg, va_list ap);
int				initialize_pl(t_pos_n_len *pl);
void			initialize_flg(t_flg *flg);
void			initialize_aff(t_aff *aff);

int				aff_c_or_percent(int c, t_flg flg, va_list ap);
int				aff_c(t_flg flg, int c);
int				number_of_byts_write_c(t_flg flg);

int				aff_s(t_flg flg, char *s);
void			aff_s_less(t_flg *flg, char *s);
void			aff_s_unless(t_flg *flg, char *s);
void			adjust_flg_n_s(t_flg *flg, char **s, int *i);
int				number_of_byts_write_s(t_flg flg);

void			adjust_flg_n_aff_p(t_aff *aff, t_flg *flg, unsigned long *i);
t_aff			adjust_aff_p(t_aff aff, t_flg flg, int nod, unsigned long i);
void			adjust_aff_p_unless(t_flg *f, t_aff *a, int n, unsigned long i);
void			aff_p_s1_z1(t_aff aff);
int				aff_p(t_flg flg, unsigned long p);
int				number_of_byts_write_p(t_aff aff, unsigned long p, t_flg flg);

int				aff_integer(char c, t_flg flg, va_list ap);

void			adjust_flg_n_aff_di(t_aff *aff, t_flg *flg, long long *i);
void			adjust_aff_di_s1_z1(t_flg flg, t_aff *aff, int nod);
void			adjust_aff_di_sign1(t_flg flg, t_aff *aff, int nod);
void			adjust_aff_di_sign2(t_flg flg, t_aff *aff, int nod);
t_aff			adjust_aff_di(t_aff aff, t_flg flg, int nod, long long i);
int				aff_di(t_flg flg, long long di);
int				number_of_byts_write_di(t_aff aff, long long di);

void			adjust_flg_n_aff_o(t_aff *aff, t_flg *flg, unsigned long *i);
t_aff			adjust_aff_o(t_aff aff, t_flg flg, int nod, unsigned long i);
void			adjust_aff_o_s1(t_flg flg, t_aff *af, int nod, unsigned long i);
void			adjust_aff_o_z1(t_flg flg, t_aff *aff, int nod);
void			aff_o_s1_z1(t_aff aff);
void			aff_o_z_arg_s2(t_aff aff, int i);
int				aff_o(t_flg flg, unsigned long o);
int				number_of_byts_write_o(t_aff aff, unsigned long o);

void			adjust_flg_n_aff_u(t_aff *aff, t_flg *flg, uintmax_t *i);
void			adjust_aff_u_sign1(t_flg flg, t_aff *aff, uintmax_t i, int nod);
void			adjust_aff_u_sign2(t_flg flg, t_aff *aff);
t_aff			adjust_aff_u(t_aff aff, t_flg flg, uintmax_t i, int nod);
void			aff_u_s1_z1(t_aff aff);
int				aff_u(t_flg flg, uintmax_t u);
int				number_of_byts_write_u(t_aff aff, uintmax_t u, t_flg flg);

void			adjust_flg_n_aff_x(t_aff *aff, t_flg *flg, unsigned long *i);
void			adjust_aff_x_s1(t_flg flg, t_aff *a, int nod, unsigned long i);
void			adjust_aff_x_z1(t_flg flg, t_aff *aff, int nod);
t_aff			adjust_aff_x(t_aff aff, t_flg flg, int nod, unsigned long i);
void			aff_x_s1_z1(t_aff aff);
int				aff_x(t_flg flg, unsigned long x);
int				number_of_byts_write_x(t_aff aff, unsigned long x, t_flg flg);

void			aff_x_maj_s1_z1(t_aff aff);
int				aff_x_maj(t_flg flg, unsigned long x);

t_aff			adjust_f_flg_n_aff(t_aff aff, t_flg *flg, float *f);
void			adjust_f_aff(t_aff *aff, t_flg flg, int nod);
char			aff_f_sign(t_aff aff, t_flg flg, int nod, int i);
int				aff_f_s1(t_aff aff, t_flg flg, int nod);
int				aff_f_z1(t_aff aff, t_flg flg, int nod);
int				aff_f(t_flg flg, float f);
int				number_of_byts_write_f(t_flg flg, float f, t_aff aff);

#endif
