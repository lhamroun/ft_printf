# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/26 15:48:38 by lyhamrou          #+#    #+#              #
#    Updated: 2019/06/05 23:05:40 by lyhamrou         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

CFLAGS = -Wall -Wextra -Werror

COMPIL = $(CC) $(CFLAGS)

EXE = a.out

MAIN = ../testeur/ft_printf/main.c

INCLUDES = -I includes/

LIB_PATH = libft/

SRC_PATH = srcs/

SRC_PATH_AFF = srcs/aff/

SRC_PATH_ADJ = srcs/adjust/

SRC_NAME = ft_printf.c initialize_struct.c vrac_functions.c

SRC_AFF = aff_c.c aff_s.c aff_p.c aff_di.c aff_o.c aff_u.c aff_x.c aff_x_maj.c \
		  aff_integer.c aff_f.c

SRC_ADJ = adjust_di.c adjust_p.c adjust_x.c adjust_u.c adjust_o.c adjust_f.c

SRC_LIB_NAME = find_char_in_str.c ft_atoi.c ft_intchr.c ft_putchar.c \
			   ft_putnbr_base_u.c ft_putnbr_base_u_maj.c ft_putnbr_l.c \
			   ft_putnbr_lu.c ft_putstr.c ft_strdup.c ft_strsub.c ft_strlen.c \
			   number_of_digit.c number_of_digit_base.c ft_memalloc.c \
			   number_of_digit_base_u.c ft_putnbr_base.c ft_strcpy.c \
			   ft_strnew.c ft_bzero.c ft_memset.c putfloat_prec.c

OBJ_PATH = .obj/

OBJ_NAME = $(addprefix $(OBJ_PATH),$(SRC_NAME:.c=.o))

OBJ_AFF = $(addprefix $(OBJ_PATH),$(SRC_AFF:.c=.o))

OBJ_ADJ = $(addprefix $(OBJ_PATH),$(SRC_ADJ:.c=.o))

OBJ_LIB_NAME = $(SRC_LIB_NAME:.c=.o)

OBJ_LIB = $(addprefix $(LIB_PATH),$(OBJ_LIB_NAME))

OBJ = $(OBJ_NAME) $(OBJ_AFF) $(OBJ_ADJ)

SRC1 = $(addprefix $(SRC_PATH),$(SRC_NAME))

SRC2 = $(addprefix $(SRC_PATH_AFF),$(SRC_AFF))

SRC3 = $(addprefix $(SRC_PATH_ADJ),$(SRC_ADJ))

SRC = $(SRC1) $(SRC2) $(SRC3)

all: $(NAME)

$(NAME): compil_lib $(OBJ) aff_obj_msg
	@echo "\033[35m\n------------\nCREATION OF $(NAME)\n------------\n\033[0m"
	@ar rc $(NAME) $(OBJ) $(OBJ_LIB)
	@ranlib $(NAME)

aff_obj_msg:
	@echo "\033[36m\n----------\nCREATION OF OBJECTS FILES\n----------\033[0m"
	@echo $(OBJ)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir -p $(OBJ_PATH)
	@$(COMPIL) $(INCLUDES) -o $@ -c $<

$(OBJ_PATH)%.o: $(SRC_PATH_AFF)%.c
	@mkdir -p $(OBJ_PATH)
	@$(COMPIL) $(INCLUDES) -o $@ -c $<

$(OBJ_PATH)%.o: $(SRC_PATH_ADJ)%.c
	@mkdir -p $(OBJ_PATH)
	@$(COMPIL) $(INCLUDES) -o $@ -c $<

compil_lib:
	@echo "\033[33m\n------------\nCOMPILATION OF LIBFT\n------------\n\033[0m"
	@make -C $(LIB_PATH)

save: fclean
	@git add .
	@git commit -m "autosave"
	@git push
	@echo "\033[34m\n------------\nGIT PUSH AUTO-SAVE\n------------\n\033[0m"

comp: re all
	@$(COMPIL) $(MAIN) $(NAME) && ./$(EXE)
	@$(RM) $(EXE)
	@make fclean

clean:
	@$(RM) -rf $(OBJ_PATH)
	@make clean -C $(LIB_PATH)

fclean: clean
	@$(RM) $(NAME)
	@make fclean -C $(LIB_PATH)

re : fclean all

.PHONY : all clean fclean ffclean re $(NAME)
