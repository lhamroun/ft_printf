/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   adjust_x.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/08 19:44:36 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/03/07 00:41:09 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_printf.h"

void	adjust_aff_p_unless(t_flg *flg, t_aff *aff, int nod, unsigned long p)
{
	if (flg->w > nod && flg->w > flg->p)
	{
		if (flg->p >= nod && flg->z == 0)
			aff->s1 = flg->w - flg->p;
		else if (flg->p < nod && flg->z == 0)
			aff->s1 = flg->w - nod;
		p == 0 && flg->p == 0 ? aff->s1++ : 1;
		aff->s1 < 0 ? (aff->s1 = 0) : 1;
	}
}

t_aff	adjust_aff_p(t_aff aff, t_flg flg, int nod, unsigned long p)
{
	if (flg.w <= nod + 2 || flg.z == 1 || flg.less == 1)
		aff.sign1 = '#';
	if (flg.less == 0 && aff.sign1 != '#')
		adjust_aff_p_unless(&flg, &aff, nod, p);
	if (flg.less == 0)
	{
		if (flg.w > nod && flg.w > flg.p)
		{
			if (flg.p > nod && flg.z == 1)
				aff.z1 = flg.w - flg.p;
			else if (flg.p < nod && flg.z == 1)
				aff.z1 = flg.w - nod;
			aff.z1 < 0 ? (aff.z1 = 0) : 1;
		}
	}
	if (aff.sign1 == '\0')
		aff.sign2 = '#';
	if (flg.less == 1 && flg.w > flg.p && flg.w > nod + 2)
		aff.s2 = flg.w - aff.s1 - aff.z1 - nod - aff.z_arg - 2;
	return (aff);
}

void	adjust_flg_n_aff_p(t_aff *aff, t_flg *flg, unsigned long *i)
{
	int		nod;

	nod = (int)number_of_digit_base_u(*i, 16);
	if (nod > flg->w && nod > flg->p && *i != 0)
	{
		flg->w = 0;
		flg->p = 0;
	}
	if (flg->less == 1 || flg->p != -1)
		flg->z = 0;
	if (flg->p >= flg->w && flg->p >= nod)
		flg->w = 0;
	if ((flg->p > flg->w) || (flg->p >= nod) || (flg->z == 1 && flg->w > nod))
		aff->z_arg = flg->p - nod;
	*aff = adjust_aff_p(*aff, *flg, nod, *i);
}
