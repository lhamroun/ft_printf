/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   adjust_f.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 04:14:01 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/03/07 07:13:30 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_printf.h"

int		aff_f_z1(t_aff aff, t_flg flg, int nod)
{
	if (flg.z == 1 && flg.less == 0 && aff.s1 == 0)
		if (flg.w > nod + aff.neg + flg.plus)
			aff.z1 = flg.w - nod - aff.neg - flg.plus;
	return (aff.z1);
}

int		aff_f_s1(t_aff aff, t_flg flg, int nod)
{
	if (flg.z == 0 && flg.less == 0)
		if (flg.w > nod + aff.neg + flg.plus)
			aff.s1 = flg.w - nod - aff.neg - flg.plus;
	return (aff.s1);
}

char	aff_f_sign(t_aff aff, t_flg flg, int nod, int i)
{
	if (i == 0 && aff.neg + flg.plus + flg.s > 0)
	{
		if (flg.w <= nod || flg.z == 1)
		{
			aff.neg == 1 ? aff.sign1 = '-' : 1;
			flg.plus == 1 ? aff.sign1 = '+' : 1;
			flg.s == 1 ? aff.sign1 = ' ' : 1;
		}
		return (aff.sign1);
	}
	if (i != 0 && aff.neg + flg.plus + flg.s > 0 && aff.sign1 == '\0')
	{
		if (flg.w > nod)
		{
			aff.neg == 1 ? aff.sign2 = '-' : 1;
			flg.plus == 1 ? aff.sign2 = '+' : 1;
			flg.s == 1 ? aff.sign2 = ' ' : 1;
		}
		return (aff.sign2);
	}
	return ('\0');
}

void	adjust_f_aff(t_aff *aff, t_flg flg, int nod)
{
	aff->sign1 = aff_f_sign(*aff, flg, nod, 0);
	aff->s1 = aff_f_s1(*aff, flg, nod);
	aff->z1 = aff_f_z1(*aff, flg, nod);
	aff->sign2 = aff_f_sign(*aff, flg, nod, 1);
	if (flg.less == 1 && flg.w > nod + aff->neg + flg.plus)
		aff->s2 = flg.w - nod - aff->neg - flg.plus - flg.s;
}

t_aff	adjust_f_flg_n_aff(t_aff aff, t_flg *flg, float *f)
{
	int nod;

	if (*f < 0)
	{
		aff.neg = 1;
		*f *= -1;
	}
	aff.neg == 1 ? flg->plus = 0 : 1;
	if (flg->p == -1)
		flg->p = 6;
	nod = (int)number_of_digit((long)*f) + flg->p + (flg->p != 0);
	if (flg->plus == 1)
		flg->s = 0;
	if (flg->less == 1)
		flg->z = 0;
	adjust_f_aff(&aff, *flg, nod);
	return (aff);
}
