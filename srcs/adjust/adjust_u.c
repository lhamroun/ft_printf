/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   adjust_u.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/12 17:40:32 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/03/06 20:07:20 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_printf.h"

void	adjust_aff_u_sign1(t_flg flg, t_aff *aff, uintmax_t i, int nod)
{
	if ((flg.less == 1 && (aff->neg + flg.plus + flg.s >= 1)) || (aff->neg == 1
	&& flg.z == 1 && flg.w > nod) || (aff->neg + nod >= flg.w || aff->neg
	+ flg.p >= flg.w) || (flg.w > flg.p && flg.w > nod && i > 0
	&& (flg.z + flg.s == 2)))
	{
		if (aff->neg == 1 && (aff->neg = 0))
			aff->sign1 = '-';
		else if (aff->neg == 0 && flg.plus == 1)
			aff->sign1 = '+';
		else if (aff->neg == 0 && flg.plus == 0 && flg.s == 1)
			aff->sign1 = ' ';
	}
	else if (flg.w < flg.p || flg.w < nod || (flg.plus == 1 && flg.w > flg.p
				&& flg.w < nod))
	{
		if (aff->neg == 1)
		{
			aff->sign1 = '-';
			aff->neg = 0;
		}
		if (aff->neg == 0 && flg.plus == 1)
			aff->sign1 = '+';
		if (aff->neg == 0 && flg.plus == 0 && flg.s == 1)
			aff->sign1 = ' ';
	}
}

void	adjust_aff_u_sign2(t_flg flg, t_aff *aff)
{
	if (flg.less == 0 && aff->sign1 == '\0' && (aff->s1 != 0 || aff->z1 != 0))
	{
		if (flg.p < flg.w)
		{
			if (aff->neg == 1)
			{
				aff->sign2 = '-';
				aff->neg = 0;
			}
			else if (aff->neg == 0 && flg.plus == 1)
				aff->sign2 = '+';
			else if (aff->neg == 0 && flg.plus == 0 && flg.s == 1)
				aff->sign2 = ' ';
		}
	}
}

void	adjust_aff_u_s1_z1(t_flg flg, t_aff *aff, int nod)
{
	if (aff->sign1 == '\0' && flg.less == 0)
	{
		if (flg.w > nod && flg.w > flg.p)
		{
			if (flg.p >= nod && flg.z == 0)
				aff->s1 = flg.w - flg.p - aff->neg;
			else if (flg.p < nod && flg.z == 0)
				aff->s1 = flg.w - nod - aff->neg;
			if ((flg.s == 1 || flg.plus == 1) && aff->neg == 0)
				aff->s1--;
		}
	}
	if (flg.less == 0 && aff->z_arg == 0)
	{
		if (flg.w > nod && flg.w > flg.p)
		{
			if (flg.p > nod && flg.z == 1)
				aff->z1 = flg.w - flg.p - aff->neg;
			else if (flg.p < nod && flg.z == 1)
				aff->z1 = flg.w - nod - aff->neg;
			if (((flg.s + flg.plus >= 1) && aff->neg == 0)
					|| aff->sign1 != '\0')
				aff->z1--;
		}
	}
}

t_aff	adjust_aff_u(t_aff aff, t_flg flg, uintmax_t i, int nod)
{
	adjust_aff_u_sign1(flg, &aff, i, nod);
	adjust_aff_u_s1_z1(flg, &aff, nod);
	adjust_aff_u_sign2(flg, &aff);
	if (flg.less == 1 && flg.w > flg.p + flg.plus && flg.w > flg.p + aff.neg)
		aff.s2 = flg.w - aff.s1 - aff.z1 - nod - aff.z_arg
			+ (i == 0 && flg.p != -1);
	return (aff);
}

void	adjust_flg_n_aff_u(t_aff *aff, t_flg *flg, uintmax_t *i)
{
	int		nod;

	nod = (int)number_of_digit_base_u((long)*i, 10);
	flg->plus = 0;
	flg->s = 0;
	flg->h = 0;
	if (flg->w < nod || flg->w < flg->p)
		flg->w = 0;
	if (flg->less == 1 || flg->p != -1)
		flg->z = 0;
	if (nod - (*i == 0) < flg->p)
	{
		aff->z_arg = flg->p - nod;
		*i == 0 ? aff->z_arg++ : 1;
	}
	else if (flg->z == 1 && flg->w > nod)
		aff->z_arg = flg->w - nod;
	*aff = adjust_aff_u(*aff, *flg, *i, nod);
}
