/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   adjust_o.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/08 03:50:32 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/03/06 20:00:53 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_printf.h"

void	adjust_aff_o_z1(t_flg flg, t_aff *aff, int nod)
{
	if (flg.less == 0 && aff->s1 == 0 && aff->z_arg == 0)
	{
		if (flg.w > nod && flg.w > flg.p)
		{
			if (flg.p > nod && flg.z == 1)
				aff->z1 = flg.w - flg.p;
			else if (flg.p < nod && flg.z == 1)
				aff->z1 = flg.w - nod - flg.h;
		}
		aff->z1 < 0 ? (aff->z1 = 0) : 1;
	}
}

void	adjust_aff_o_s1(t_flg flg, t_aff *aff, int nod, unsigned long i)
{
	if (aff->sign1 == '\0' && flg.less == 0)
	{
		if (flg.w > nod && flg.w > flg.p)
		{
			if (flg.p > nod - (flg.h > 0) && flg.z == 0)
				aff->s1 = flg.w - flg.p;
			else if (flg.p <= nod - (flg.h > 0) && flg.z == 0)
				aff->s1 = flg.w - nod;
			if (flg.h == 1 && i != 0)
				aff->s1--;
		}
		aff->s1 < 0 ? (aff->s1 = 0) : 1;
	}
}

t_aff	adjust_aff_o(t_aff aff, t_flg flg, int nod, unsigned long i)
{
	if (flg.h == 1)
		if (flg.w <= nod + aff.z_arg + 1 - (i == 0) || flg.w < flg.p + 1
				|| flg.less == 1 || flg.z == 1)
			aff.sign1 = '0';
	adjust_aff_o_s1(flg, &aff, nod, i);
	adjust_aff_o_z1(flg, &aff, nod - (i == 0 && flg.h == 1));
	if (flg.less == 0 && aff.sign1 == '\0' && (aff.s1 != 0 || aff.z1 != 0))
	{
		if (flg.p < flg.w)
			if (flg.h == 1)
				aff.sign2 = '0';
	}
	if (flg.less == 1 && flg.w > flg.p)
		aff.s2 = flg.w - (aff.sign1 != '\0') - (aff.sign2 != '\0')
			- (nod * (i != 0 || (i == 0 && flg.p < 1 && flg.h == 0)))
			- aff.z_arg;
	aff.s2 < 0 ? (aff.s2 = 0) : 1;
	aff.z1 < 0 ? (aff.z1 = 0) : 1;
	aff.z_arg < 0 ? (aff.z_arg = 0) : 1;
	return (aff);
}

void	adjust_flg_n_aff_o(t_aff *aff, t_flg *flg, unsigned long *i)
{
	int		nod;

	nod = (int)number_of_digit_base_u(*i, 8);
	if (nod > flg->w && nod > flg->p && *i != 0)
	{
		flg->w = 0;
		flg->p = 0;
	}
	if (flg->less == 1 || flg->p != -1)
		flg->z = 0;
	if (flg->p >= flg->w && flg->p >= nod)
		flg->w = 0;
	if (flg->p > flg->w || flg->p >= nod)
		aff->z_arg = flg->p - nod + (*i == 0);
	aff->z_arg < 0 ? (aff->z_arg = 0) : 1;
	aff->z_arg > 0 ? (flg->h = 0) : 1;
	*aff = adjust_aff_o(*aff, *flg, nod, *i);
}
