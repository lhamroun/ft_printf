/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   adjust_di.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/31 17:18:01 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/03/06 20:01:35 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_printf.h"

void	adjust_aff_di_s1_z1(t_flg flg, t_aff *aff, int nod)
{
	if (aff->sign1 == '\0' && flg.less == 0)
	{
		if (flg.w > nod && flg.w > flg.p)
		{
			if (flg.p >= nod && flg.z == 0)
				aff->s1 = flg.w - flg.p - aff->neg;
			else if (flg.p < nod && flg.z == 0)
				aff->s1 = flg.w - nod;
			if ((flg.s == 1 || flg.plus == 1) && aff->neg == 0)
				aff->s1--;
		}
	}
	if (flg.less == 0 && aff->z_arg == 0 && aff->s1 <= 0)
	{
		if (flg.w > nod && flg.w > flg.p)
		{
			if (flg.p > nod && flg.z == 1)
				aff->z1 = flg.w - flg.p - aff->neg;
			else if (flg.p < nod && flg.z == 1)
				aff->z1 = flg.w - nod - aff->neg;
			if (((flg.s == 1 || flg.plus == 1) && aff->neg == 0)
					|| aff->sign1 != '\0')
				aff->z1--;
		}
	}
}

void	adjust_aff_di_sign2(t_flg flg, t_aff *aff, int nod)
{
	aff->s1 < 0 ? (aff->s1 = 0) : 1;
	aff->z1 < 0 ? (aff->z1 = 0) : 1;
	if (flg.less == 0 && aff->sign1 == '\0' && aff->s1 + aff->z1 > 0)
	{
		if (flg.p < flg.w || flg.w > nod)
		{
			if (aff->neg == 1)
				aff->sign2 = '-';
			else if (aff->neg == 0 && flg.plus == 1)
				aff->sign2 = '+';
			else if (aff->neg == 0 && flg.plus == 0 && flg.s == 1)
				aff->sign2 = ' ';
		}
	}
}

void	adjust_aff_di_sign1(t_flg flg, t_aff *aff, int nod)
{
	if (flg.plus == 1 || flg.s == 1 || aff->neg == 1)
	{
		if (flg.less == 1 || flg.z == 1 || flg.w <= flg.p + aff->neg
				|| flg.w <= nod + aff->z_arg + (flg.plus + flg.s == 1))
		{
			if (flg.plus == 1)
				aff->sign1 = '+';
			else if (flg.s == 1)
				aff->sign1 = ' ';
			else
				aff->sign1 = '-';
		}
	}
}

t_aff	adjust_aff_di(t_aff aff, t_flg flg, int nod, long long i)
{
	adjust_aff_di_sign1(flg, &aff, nod - (i == 0 && flg.p != -1));
	adjust_aff_di_s1_z1(flg, &aff, nod);
	adjust_aff_di_sign2(flg, &aff, nod);
	if (flg.less == 1 && flg.w > flg.p + flg.plus && flg.w > flg.p + aff.neg)
		aff.s2 = flg.w - (aff.sign1 != '\0') - aff.s1 - aff.z1
			- (aff.sign2 != '\0') - nod - aff.z_arg + aff.neg
			+ (i == 0 && flg.p != -1);
	return (aff);
}

void	adjust_flg_n_aff_di(t_aff *aff, t_flg *flg, long long *i)
{
	int		nod;

	nod = (int)number_of_digit((long)*i);
	if (*i < 0)
		flg->plus = 0;
	if (flg->plus == 1 || *i < 0)
		flg->s = 0;
	if (flg->w <= nod || flg->w <= flg->p)
		flg->w = 0;
	if (flg->less == 1 || flg->p != -1)
		flg->z = 0;
	if (nod - (*i == 0) < flg->p + (*i < 0) + flg->plus + flg->s)
	{
		aff->z_arg = flg->p - nod;
		*i < 0 ? aff->z_arg++ : 1;
		*i == 0 ? aff->z_arg++ : 1;
	}
	else if (flg->z == 1 && flg->w > nod + (flg->plus == 1) + (flg->s == 1))
		aff->z_arg = flg->w - nod - (flg->plus == 1) - (flg->s == 1);
	if (*i < 0)
	{
		aff->neg = 1;
		*i *= -1;
	}
	*aff = adjust_aff_di(*aff, *flg, nod, *i);
}
