/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff_di.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/31 16:47:30 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/03/03 21:06:21 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_printf.h"

void	aff_di_s1_x1(t_aff aff)
{
	while (aff.s1 > 0 || aff.z1 > 0)
	{
		if (aff.z1 > 0)
			write(1, "0", 1);
		else if (aff.s1 > 0)
			write(1, " ", 1);
		aff.s1--;
		aff.z1--;
	}
}

int		aff_di(t_flg flg, long long di)
{
	int		i;
	t_aff	aff;

	initialize_aff(&aff);
	adjust_flg_n_aff_di(&aff, &flg, &di);
	if (di != LONG_MIN)
		aff.sign1 == '\0' ? 1 : ft_putchar(aff.sign1);
	i = number_of_byts_write_di(aff, di);
	aff_di_s1_x1(aff);
	di == 0 && flg.p == 0 && flg.w > flg.p ? write(1, " ", 1) && ++i : 1;
	aff.sign2 == '\0' ? 1 : ft_putchar(aff.sign2);
	while (aff.z_arg > 0)
	{
		write(1, "0", 1);
		aff.z_arg--;
	}
	di != 0 || (di == 0 && flg.p == -1) ? ft_putnbr_l(di) : 1;
	di == 0 && flg.p != -1 ? --i : 1;
	while (aff.s2 > 0)
	{
		write(1, " ", 1);
		aff.s2--;
	}
	return (i);
}

int		number_of_byts_write_di(t_aff aff, long long di)
{
	int		sign;
	int		arg;
	int		padding;
	int		zero;

	sign = (aff.sign1 != '\0') + (aff.sign2 != '\0');
	arg = (int)number_of_digit(di);
	padding = aff.s1 + aff.s2;
	zero = aff.z1 + aff.z_arg;
	if (padding < 0)
		padding = 0;
	if (zero < 0)
		zero = 0;
	di == LONG_MIN ? sign-- : 1;
	return (sign + arg + padding + zero);
}
