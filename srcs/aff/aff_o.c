/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff_o.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 18:26:52 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/03/06 20:00:13 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_printf.h"

void	aff_o_z_arg_s2(t_aff aff, int i)
{
	if (i == 0)
	{
		while (aff.z_arg > 0)
		{
			write(1, "0", 1);
			aff.z_arg--;
		}
	}
	else if (i == 1)
	{
		while (aff.s2 > 0)
		{
			write(1, " ", 1);
			aff.s2--;
		}
	}
}

void	aff_o_s1_z1(t_aff aff)
{
	while (aff.s1 > 0 || aff.z1 > 0)
	{
		if (aff.z1 > 0)
			write(1, "0", 1);
		else if (aff.s1 > 0)
			write(1, " ", 1);
		aff.s1--;
		aff.z1--;
	}
}

int		aff_o(t_flg flg, unsigned long o)
{
	int		i;
	t_aff	aff;

	initialize_aff(&aff);
	adjust_flg_n_aff_o(&aff, &flg, &o);
	i = number_of_byts_write_o(aff, o);
	aff.sign1 == '\0' ? 1 : ft_putchar(aff.sign1);
	aff_o_s1_z1(aff);
	o == 0 && flg.p == 0 && flg.w > flg.p ? write(1, " ", 1) && ++i : 1;
	aff.sign2 == '\0' ? 1 : ft_putchar(aff.sign2);
	aff_o_z_arg_s2(aff, 0);
	o != 0 ? ft_putnbr_base_u(o, 8) : 1;
	o == 0 && flg.p == -1 && flg.h == 0 && (flg.w > 0 || flg.h == 0)
		? write(1, "0", 1) && ++i : 1;
	aff_o_z_arg_s2(aff, 1);
	return (i);
}

int		number_of_byts_write_o(t_aff aff, unsigned long o)
{
	int		sign;
	int		arg;
	int		padong;
	int		zero;

	aff.s1 < 0 ? (aff.s1 = 0) : 1;
	aff.z1 < 0 ? (aff.z1 = 0) : 1;
	aff.s2 < 0 ? (aff.s2 = 0) : 1;
	aff.z_arg < 0 ? (aff.z_arg = 0) : 1;
	sign = (aff.sign1 != '\0') + (aff.sign2 != '\0');
	arg = (int)number_of_digit_base_u(o, 8) - (o == 0);
	padong = aff.s1 + aff.s2;
	zero = aff.z1 + aff.z_arg;
	return (sign + arg + padong + zero);
}
