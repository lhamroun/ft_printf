/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff_f.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/31 18:09:10 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/03/07 07:13:41 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_printf.h"

int		aff_f(t_flg flg, float f)
{
	int		i;
	t_aff	aff;

	initialize_aff(&aff);
	aff = adjust_f_flg_n_aff(aff, &flg, &f);
	i = number_of_byts_write_f(flg, f, aff);
	aff.sign1 != '\0' ? write(1, &aff.sign1, 1) : 1;
	while (aff.s1 > 0 || aff.z1 > 0)
	{
		aff.s1 > 0 ? write(1, " ", 1) : 1;
		aff.z1 > 0 ? write(1, "0", 1) : 1;
		aff.s1--;
		aff.z1--;
	}
	aff.sign2 != '\0' ? write(1, &aff.sign2, 1) : 1;
	putfloat_prec(f, (unsigned int)flg.p);
	while (aff.s2 > 0)
		aff.s2 > 0 ? write(1, " ", 1) && aff.s2-- : 1;
	return (i);
}

int		number_of_byts_write_f(t_flg flg, float f, t_aff aff)
{
	int		i;

	i = (int)number_of_digit((long)f) + flg.p + (flg.p != 0)
		+ flg.plus + aff.neg + flg.s;
	i < flg.w ? (i = flg.w) : 1;
	return (i);
}
