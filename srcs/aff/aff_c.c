/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff_c.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/31 16:48:38 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/02/25 19:56:34 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_printf.h"

int			aff_c(t_flg flg, int c)
{
	int		i;

	i = number_of_byts_write_c(flg);
	if (flg.less == 1)
	{
		ft_putchar(c);
		while (flg.w > 1)
		{
			write(1, " ", 1);
			flg.w--;
		}
	}
	else if (flg.less == 0)
	{
		while (flg.w > 1)
		{
			flg.z == 0 ? write(1, " ", 1) : write(1, "0", 1);
			flg.w--;
		}
		ft_putchar(c);
	}
	return (i);
}

int			number_of_byts_write_c(t_flg flg)
{
	if (flg.w > 1)
		return (flg.w);
	return (1);
}

int			aff_c_or_percent(int c, t_flg flg, va_list ap)
{
	int		i;

	i = 0;
	if (c == 'c')
		i = aff_c(flg, va_arg(ap, int));
	else if (c == '%')
		i = aff_c(flg, 37);
	else
		i = -1;
	return (i);
}
