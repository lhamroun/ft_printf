/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff_s.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/31 16:52:19 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/02/26 16:39:43 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/ft_printf.h"

void	aff_s_unless(t_flg *flg, char *s)
{
	while (flg->w > flg->p)
	{
		if (flg->z == 1)
			write(1, "0", 1);
		else
			write(1, " ", 1);
		flg->w--;
	}
	write(1, s, flg->p);
}

void	aff_s_less(t_flg *flg, char *s)
{
	write(1, s, flg->p);
	while (flg->w - flg->p + 1 > 1)
	{
		write(1, " ", 1);
		flg->w--;
	}
}

void	adjust_flg_n_s(t_flg *flg, char **s, int *i)
{
	flg->p > (int)ft_strlen(*s) ? (flg->p = (int)ft_strlen(*s)) : 1;
	flg->p == -1 ? (flg->p = (int)ft_strlen(*s)) : 1;
	*i = number_of_byts_write_s(*flg);
}

int		aff_s(t_flg flg, char *s)
{
	int		i;
	char	*n;

	i = 0;
	n = "(null)";
	if (s == NULL)
		s = n;
	adjust_flg_n_s(&flg, &s, &i);
	if (flg.less == 1)
		aff_s_less(&flg, s);
	else if (flg.less == 0)
		aff_s_unless(&flg, s);
	return (i);
}

int		number_of_byts_write_s(t_flg flg)
{
	int		i;

	i = flg.p;
	if (flg.w > i)
		return (flg.w);
	else
		return (i);
}
