/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/08 19:42:53 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/03/07 01:16:52 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int			flag_type(char const *f, int i, int j)
{
	while (i < j)
	{
		if (f[i] == 'h')
		{
			if (f[i + 1] == 'h')
				return (2);
			return (1);
		}
		else if (f[i] == 'l')
		{
			if (f[i + 1] == 'l')
				return (4);
			return (3);
		}
		else if (f[i] == 'j')
			return (5);
		else if (f[i] == 'z')
			return (6);
		++i;
	}
	return (0);
}

void		pars_flag(t_flg *flg, char const *f, int i, int j)
{
	int		x;

	x = i;
	i = pars_format_flag(flg, f, i, j);
	while (f[i - 1] && (f[i - 1] == 'h' || f[i - 1] == 'l' || f[i - 1] == 'j'
				|| f[i - 1] == 'z'))
		--i;
	while (f[i - 1] && (f[i - 1] >= '0' && f[i - 1] <= '9'))
		--i;
	while (i < j && (f[i] >= '0' && f[i] <= '9'))
		flg->w = flg->w * 10 + f[i++] - '0';
	if (f[i] == '.')
		flg->p = ft_atoi((char *)&f[i + 1]);
	flg->t = flag_type(f, x, j);
}

int			find_arg_n_aff(char c, va_list ap, t_flg flg)
{
	int		i;

	i = 0;
	if (c == 'c' || c == '%')
		i = aff_c_or_percent(c, flg, ap);
	else if (c == 's')
		i = aff_s(flg, va_arg(ap, char *));
	else if (c == 'p')
		i = aff_p(flg, va_arg(ap, long));
	else if (find_char_in_str(c, "diouxX") == 1)
		i = aff_integer(c, flg, ap);
	else if (c == 'f')
		i = aff_f(flg, va_arg(ap, double));
	else
		i = -1;
	return (i);
}

t_pos_n_len	parse_pos_len(char const *f, t_pos_n_len *pl, va_list ap)
{
	int		pos;
	int		x;
	t_flg	flg;

	if ((pos = ft_intchr(&f[pl->p + 1], "cspdiouxXf%")) == -1)
		return (error(*pl));
	initialize_flg(&flg);
	pars_flag(&flg, f, pl->p, pl->p + pos + 1);
	x = find_arg_n_aff(f[pl->p + pos + 1], ap, flg);
	pl->l = pl->l + x;
	pl->p = pl->p + pos + 2;
	if (x == -1)
	{
		pl->l = -1;
		pl->p = -1;
	}
	return (*pl);
}

int			ft_printf(char const *format, ...)
{
	int			i;
	t_pos_n_len	pl;
	va_list		ap;

	va_start(ap, format);
	i = initialize_pl(&pl);
	while (format[pl.p] != '\0')
	{
		i = pl.p;
		while (format[pl.p] != '%' && format[pl.p] != '\0')
			pl.p++;
		write(1, &format[i], pl.p - i);
		pl.l = pl.l + pl.p - i;
		if (format[pl.p] == '%')
			pl = parse_pos_len(format, &pl, ap);
		if (pl.p == -1 || pl.l == -1)
			return (0);
	}
	va_end(ap);
	return (pl.l);
}
