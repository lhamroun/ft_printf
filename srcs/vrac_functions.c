/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vrac_functions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/25 18:13:17 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/26 13:30:04 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int		pars_format_flag(t_flg *flg, char const *f, int i, int j)
{
	while (i < j)
	{
		if (f[i] == '#')
			flg->h = 1;
		if (f[i] == '-')
			flg->less = 1;
		if (f[i] == '+')
			flg->plus = 1;
		if (f[i] == '0' && (!(f[i - 1] >= '0' && f[i - 1] <= '9')))
			flg->z = 1;
		if (f[i] == ' ')
			flg->s = 1;
		if (f[i] == '.')
			break ;
		++i;
	}
	return (i);
}

int		convert_int_to_l(char c, t_flg flg, va_list ap)
{
	int i;

	i = 0;
	if (c == 'd' || c == 'i')
		i = aff_di(flg, (__int128_t)va_arg(ap, long));
	else if (c == 'o')
		i = aff_o(flg, va_arg(ap, unsigned long));
	else if (c == 'u')
		i = aff_u(flg, va_arg(ap, unsigned long));
	else if (c == 'x')
		i = aff_x(flg, va_arg(ap, unsigned long));
	else if (c == 'X')
		i = aff_x_maj(flg, va_arg(ap, unsigned long));
	return (i);
}

int		convert_int_to_h(char c, t_flg flg, va_list ap)
{
	int i;

	i = 0;
	if (c == 'd' || c == 'i')
		i = aff_di(flg, (short)va_arg(ap, int));
	else if (c == 'o')
		i = aff_o(flg, (unsigned short)va_arg(ap, unsigned int));
	else if (c == 'u')
		i = aff_u(flg, (unsigned short)va_arg(ap, unsigned int));
	else if (c == 'x')
		i = aff_x(flg, (unsigned short)va_arg(ap, unsigned int));
	else if (c == 'X')
		i = aff_x_maj(flg, (unsigned short)va_arg(ap, unsigned int));
	return (i);
}
